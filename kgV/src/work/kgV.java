package work;

public class kgV {

	/**
	 * findet das kleinste gemeinsame Vielf�che 
	 * 
	 * @param int a
	 * @param int b
	 * @return Integer
	 */
	public static Integer kgV(int a, int b) {
		if(a==0 || b==0)
			return null;
		if(a<0)
			a*=-1;
		if(b<0)
			b*=-1;
		int i=1;
		while(true) {
			int temp=i*a;
			if(temp%b==0) 
				return temp;
			i++;
		}
	}
}
