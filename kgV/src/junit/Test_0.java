package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import work.kgV;

public class Test_0 {

	@Test
	public void testZahlNull1() {
		assertEquals(null, kgV.kgV(0, 4));
	}

	@Test
	public void testZahlNull2() {
		assertEquals(null, kgV.kgV(0, 0));
	}

	@Test
	public void testNegativZahl1() {
		assertEquals(3, kgV.kgV(-1, 3).intValue());
	}

	@Test
	public void testNegativZahl2() {
		assertEquals(24, kgV.kgV(-3, -8).intValue());
	}

	@Test
	public void testNormal1() {
		assertEquals(210, kgV.kgV(7, 30).intValue());
	}

	@Test
	public void testNormal2() {
		assertEquals(14, kgV.kgV(7, 14).intValue());
	}

	@Test
	public void testNormal3() {
		assertEquals(9, kgV.kgV(9, 9).intValue());
	}

	@Test
	public void testNormal4() {
		assertEquals(361862, kgV.kgV(986, 367).intValue());
	}
	
	
}
