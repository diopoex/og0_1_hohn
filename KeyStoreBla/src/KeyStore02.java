import java.util.ArrayList;
public class KeyStore02 {
	private ArrayList<String> keyList;
	
	public KeyStore02() {
		this.keyList= new ArrayList<String>();
	}
	
	public boolean add(String str) {
		this.keyList.add(str);
		return true;
	}
	
	public int indexOf(String str) {
		return this.keyList.indexOf(str);
	}
	
	public void remove(int i) {
		this.keyList.remove(i);
	}
	
	public String toString() {
		String str="";
		for(int i=0;i<keyList.size();i++) {
			str+=keyList.get(i)+", ";
		}
		return str;
	}
}
