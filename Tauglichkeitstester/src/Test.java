
public abstract class Test {
	public Test() {
		this.setAktiv(false);
		this.ergebnis=null;
	}
	private boolean aktiv;
	private Double ergebnis;
	public abstract void starten();
	public abstract void stoppen();
	public abstract String zeigeHilfe();
	public boolean isAktiv() {
		return aktiv;
	}
	public void setAktiv(boolean aktiv) {
		this.aktiv = aktiv;
	}
}



