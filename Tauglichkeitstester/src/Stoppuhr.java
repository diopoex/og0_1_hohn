import java.time.Instant;
// IllegalStateException importieren dann throw an //
public class Stoppuhr {
	public Stoppuhr() {
		this.startZeit = null;
		this.stoppZeit = null;
	}

	private Long startZeit;
	private Long stoppZeit;

	public void reset() {
		this.startZeit = null;
		this.stoppZeit = null;
	}

	public void start() {
		if(startZeit!=null) {
			System.out.println("Stoppuhr reset");
			this.reset();
			//
		}
		startZeit = Instant.now().toEpochMilli();
	}

	public void stopp() {
		if(stoppZeit!=null) {
			System.out.println("Stoppuhr reset");
			this.reset();
			//
		}
		stoppZeit = Instant.now().toEpochMilli();
	}
	public long getMilliTime() {
		if(stoppZeit==null) {
			this.stopp();
		}
		return stoppZeit-startZeit;
	}
	public double getSecTime() {
		if(stoppZeit==null) {
			this.stopp();
		}
		return (double) (stoppZeit-startZeit) / 1000;
	}
}
