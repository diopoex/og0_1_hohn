import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.ScrollPane;
import java.awt.TextArea;

public class Window {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// einfach bei auswahl mouse listener adden
		// bzw. + entfernen
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		ButtonGroup bGroup = new ButtonGroup();
		
		JRadioButton rboReaktion = new JRadioButton("Reaktion");
		rboReaktion.setSelected(true);
		rboReaktion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Reaktion");
			}
		});
		rboReaktion.setBounds(34, 161, 109, 23);
		bGroup.add(rboReaktion);
		frame.getContentPane().add(rboReaktion);
		
		JRadioButton rboEinschaetzung = new JRadioButton("Einsch\u00E4tzung");
		rboEinschaetzung.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Einsch\u00E4tzung");
				
			}
		});
		rboEinschaetzung.setBounds(34, 187, 109, 23);
		bGroup.add(rboEinschaetzung);
		frame.getContentPane().add(rboEinschaetzung);
		
		JRadioButton rboKonzentration = new JRadioButton("Konzentration");
		rboKonzentration.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Konzentration");
			}
		});
		rboKonzentration.setBounds(34, 213, 109, 23);
		bGroup.add(rboKonzentration);
		frame.getContentPane().add(rboKonzentration);
		
		TextArea textArea = new TextArea();
		textArea.setBounds(212, 10, 193, 126);
		frame.getContentPane().add(textArea);
	}
}
