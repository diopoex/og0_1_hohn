package mergesort;

import java.util.Arrays;

public class mainV {
	public static int vertauschungen = 0;
	public static int vergleiche = 0;

	public static void main(String[] args) {
		int[] arrUnsortiert = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };
		int[] arrHalbSortiert = { 1, 2, 3, 4, 5, 9, 8, 7, 6 };
		int[] arrSortiert = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		printGood(arrUnsortiert);
	}

	public static int[] mergeSort(int[] arrara, int l, int r) {
		if (l == r)
			return new int[] { arrara[l] };
		int mid = l + (r - l) / 2;
		int[] lArr = mergeSort(arrara, l, mid);
		int[] rArr = mergeSort(arrara, mid + 1, r);
		System.out.println("Auseinandergenommen: " + Arrays.toString(lArr) + " " + Arrays.toString(rArr));
		return merge(lArr, rArr);
	}

	public static int[] merge(int[] lArr, int[] rArr) {
		int[] arrMerged = new int[(lArr.length + rArr.length)];
		int tPos = 0;
		int lPos = 0;
		int rPos = 0;

		while (lPos < lArr.length && rPos < rArr.length) {
			int lVal = lArr[lPos];
			int rVal = rArr[rPos];
			if (lVal <= rVal) {
				arrMerged[tPos++] = lVal;
				lPos++;
				vergleiche++;
			} else {
				arrMerged[tPos++] = rVal;
				rPos++;
				vergleiche++;
				vertauschungen++;
			}
		}
		while (lPos < lArr.length) {
			vergleiche++;
			arrMerged[tPos++] = lArr[lPos++];
		}

		while (rPos < rArr.length) {
			vergleiche++;
			arrMerged[tPos++] = rArr[rPos++];
		}

		System.out.println("Zusammengesetzt:     " + Arrays.toString(arrMerged));
		return arrMerged;
	}

	public static void printGood(int[] arr) {
		System.out.println("MergeSort:\n-------\nStart                " + Arrays.toString(arr));
		System.out.println("Ende                 " + Arrays.toString(mergeSort(arr, 0, arr.length - 1)));
		System.out.println("Vertauschungen: "+vertauschungen);
		System.out.println("Vergleiche:     "+vergleiche);
	}
}
