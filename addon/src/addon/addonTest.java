package addon;

import java.util.Scanner;

public class addonTest {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		String bezeichnung;
		String art;
		double preis;
		int id;
		int anzahl;
		int maxAnzahl;
		
		System.out.println("TEST-PROGRAMM F�R ADDON\nGeben sie die Bezeicnung des In-App kaufes an");
		bezeichnung=input.next();
		System.out.println("Geben sie die art des In-App kaufes an");
		art=input.next();
		System.out.println("Geben sie den Preis des In-App kaufes an");
		preis=input.nextDouble();
		System.out.println("Geben sie ID des In-App kaufes an");
		id=input.nextInt();
		System.out.println("Geben an wie oft sie den In-App kauf kaufen wollen\n(Oder 0 eingeben)");
		anzahl=input.nextInt();
		System.out.println("Geben sie an wie oft der K�ufer den In-App Kauf kaufen kann");
		maxAnzahl=input.nextInt();
		
		addon addon01=new addon(bezeichnung,art,preis,id,anzahl,maxAnzahl);
		System.out.println("Ihr In-App Kauf hei�t: "+addon01.getBezeichnung()+"\nDie Art des In-App ist: "+addon01.getArt()+"\nDer In-App Kauf kostet: "+addon01.getPreis()+"\nDie Id des In-App Kauf ist: "+addon01.getId()+"\nSie haben "+addon01.getAnzahl()+" In-App K�ufe get�tigt\nSie k�nnen maximal "+addon01.getMaxAnzahl()+" In-App K�ufe t�tigen");
		
		addon addon02=new addon("BOOST", "Tierfutter", 5.99, 92584682, 0,15);
		System.out.println("\n\n\n\nIhr In-App Kauf hei�t: "+addon02.getBezeichnung()+"\nDie Art des In-App ist: "+addon02.getArt()+"\nDer In-App Kauf kostet: "+addon02.getPreis()+"\nDie Id des In-App Kauf ist: "+addon02.getId()+"\nSie haben "+addon02.getAnzahl()+" In-App K�ufe get�tigt\nSie k�nnen maximal "+addon02.getMaxAnzahl()+" In-App K�ufe t�tigen");
		input.close();
	}
}
