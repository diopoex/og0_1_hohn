package addon;

public class addon {
	private String bezeichnung;
	private String art;
	private double preis;
	private int id;
	private int anzahl;
	private int maxAnzahl;

	public addon() {

	}

	public addon(String bezeichnung, String art, double preis, int id, int anzahl, int maxAnzahl) {
		this.bezeichnung = bezeichnung;
		this.art = art;
		this.preis = preis;
		this.id = id;
		this.anzahl = anzahl;
		this.maxAnzahl = maxAnzahl;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getArt() {
		return art;
	}

	public void setArt(String art) {
		this.art = art;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public int getMaxAnzahl() {
		return maxAnzahl;
	}

	public void setMaxAnzahl(int maxAnzahl) {
		this.maxAnzahl = maxAnzahl;
	}

	public void useAddon(int i) {
		this.anzahl -= i; // theoretisch m�sste noch eine if verschachtelung hinzugef�gt werden falls die
							// anzahl dann kleiner als 0
	}

	public void addAddon(int i) {
		this.anzahl += i; // theoretisch m�sste noch eine if verschachtelung hinzugef�gt werden falls die
							// anzahl dann gr��er als maxAnzahl ist
	}

	public double gesamtwertAddons() {
		return anzahl*preis;
	}

}
