package de.oszimt.starsim2099;

public abstract class PositionenSuper {
	private double posX;
	private double posY;

	public PositionenSuper() {
		super();
	}

	public PositionenSuper(double posX, double posY) {
		super();
		this.posX = posX;
		this.posY = posY;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

}
