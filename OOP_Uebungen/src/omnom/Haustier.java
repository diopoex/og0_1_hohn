package omnom;

public class Haustier {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;

	public Haustier() {
		this.hunger=100;
		this.muede=100;
		this.zufrieden=100;
		this.gesund=100;
	}
	public Haustier(String name) {
		this.name=name;
		this.hunger=100;
		this.muede=100;
		this.zufrieden=100;
		this.gesund=100;
	}
	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (hunger>=0 && hunger<=100) {
			this.hunger=hunger;
		} else if (hunger<0) {
			this.hunger=0;
		}else{
			this.hunger=100;
		}
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (muede>=0 && muede<=100) {
			this.muede=muede;
		} else if (muede<0) {
			this.muede=0;
		}else{
			this.muede=100;
		}
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden>=0 && zufrieden<=100) {
			this.zufrieden=zufrieden;
		} else if (zufrieden<0) {
			this.zufrieden=0;
		}else{
			this.zufrieden=100;
		}
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (gesund>=0 && gesund<=100) {
			this.gesund=gesund;
		} else if (gesund<0) {
			this.gesund=0;
		}else{
			this.gesund=100;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void fuettern(int anzahl) { //cap 100
		if (this.hunger+anzahl>100) {
			this.hunger=100;
		} else if (this.hunger+anzahl<0){
			this.hunger=0;
		} else {
			this.hunger+=anzahl;
		}
	}

	public void schlafen(int dauer) {
		if (this.muede+dauer>100) {
			this.muede=100;
		} else if (this.muede+dauer<0){
			this.muede=0;
		} else {
			this.muede+=dauer;
		}
	}

	public void spielen(int dauer) {
		if (this.zufrieden+dauer>100) {
			this.zufrieden=100;
		} else if (this.zufrieden+dauer<0){
			this.zufrieden=0;
		} else {
			this.zufrieden+=dauer;
		}
	}

	public void heilen() {
		this.gesund=100;
	}

}
