import java.time.Duration;
import java.time.LocalTime;

public class tres {
	public static void main(String[] args) {
		long[] arr = { 14523493, 84523463, 245234837, 645234553, 845234909, 2452345331L, 6452345423L };
		Primzahl prim = new Primzahl();

		for (int i = 0; i < arr.length; i++) {
			LocalTime zeit1 = LocalTime.now();
			System.out.println(arr[i] + " " + prim.isPrimzahl(arr[i]));
			LocalTime zeit2 = LocalTime.now();
			calculateTimeDiff(zeit1, zeit2);
		}
	}

	public static void calculateTimeDiff(LocalTime zeit1, LocalTime zeit2) {
		Duration d1 = Duration.between(zeit1, zeit2);
		System.out.println(d1.getSeconds() + "," + d1.getNano());
	}
}
