

import java.util.Arrays;

public class aufgabe3Methoden {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] zahlen = { 1, 2, 3, 4, 5, 6 };
		System.out.println(Arrays.toString(umdrehenArray(zahlen)));
	}

	public static int[] umdrehenArray(int[] zahlen) {
		int[] zahlen2=new int[zahlen.length];
		int y=0;
		for (int i = zahlen.length - 1; i >= 0; i--) {
			zahlen2[y]=zahlen[i];
			y++;
		}
		return zahlen2;
	}

}
