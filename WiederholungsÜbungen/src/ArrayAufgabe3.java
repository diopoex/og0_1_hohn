

public class ArrayAufgabe3 {
  public static void main(String[] args) {
    // 1. Deklaration eines Arrays f�r ganze Zahlen.
    int[] zahlen = new int[100];
    // 2. Initialisierung des Arrays mit 100 Elementen.
    int q;
    for (int i = 0; i < zahlen.length; i++) {
      q = (int) (Math.floor((Math.random() * 100) + 1));
      zahlen[i] = q;
    }
    // 3. Durchlaufen des gesamten Arrays und Ausgabe des Inhalts.
    for (int i = 0; i < zahlen.length; i++) {
      System.out.println(zahlen[i]);
    }
    // 4. Das Array mit allen ganzen Zahlen von 1 bis 100 bef�llen.
    for (int i = 0; i < zahlen.length; i++) {
      zahlen[i] = i;
    }
    // 5. Geben Sie den Wert an der Stelle 89 des Arrays aus.
    System.out.println(zahlen[88]);
    // 6. �ndern Sie den Wert des Arrayelements mit dem Index 49 zu 1060.
    zahlen[49] = 1060;
    // Au�erdem �ndern Sie den Wert an der ersten und der letzte Stelle des Arrays
    // zu 2020.
    zahlen[0] = 2020;
    zahlen[zahlen.length - 1] = 2020;
    // 7. Erneutes ausgeben des Arrayinhalts.
    // Darstellung: Index und zugeh�riger Inhalt (z.B. Index 6 - Inhalt: 7)
    for (int i = 0; i < zahlen.length; i++) {
      System.out.println("Index " + i + " - Inhalt: " + zahlen[i]);
    }
    // 8. Berechnung des Durchschnitts aller Arrayelemente
    int x = 0;
    for (int j = 0; j < zahlen.length; j++) {
      x += zahlen[j];
    }
    x /= zahlen.length;
  }
}
