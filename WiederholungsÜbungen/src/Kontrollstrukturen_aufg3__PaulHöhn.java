

import java.text.DecimalFormat;
import java.util.Scanner;

public class Kontrollstrukturen_aufg3__PaulHöhn {
	public static void main(String[] args) {
		DecimalFormat f = new DecimalFormat("#0.00");
		Scanner input=new Scanner(System.in);
		System.out.println("Geben sie ihr Körpergewicht in kg an");
		double gewicht=input.nextDouble();
		System.out.println("Geben sie ihre größe in metern an");
		double groesze=input.nextDouble();
		System.out.println("Geben sie ihr Geschlecht an(m/w)");
		char geschlecht=input.next().charAt(0);
		double bmiZahl=gewicht/Math.pow(groesze, 2);
		String bmi;
		if(geschlecht=='m') {
			bmi=mBMI(bmiZahl);
		} else {
			bmi=wBMI(bmiZahl);
		}
		System.out.println("Sie haben einen Body Mass Index von "+f.format(bmiZahl)+" und somit ein "+bmi);
		input.close();
	}

	public static String mBMI(double bmiZahl) {
		if (bmiZahl < 20) {
			return "Untergewicht";
		} else if (bmiZahl <= 25) {
			return "Normalgewicht";
		} else {
			return "Übergewicht";
		}
	}
	public static String wBMI(double bmiZahl) {
		if (bmiZahl < 19) {
			return "Untergewicht";
		} else if (bmiZahl <= 24) {
			return "Normalgewicht";
		} else {
			return "Übergewicht";
		}
	}
}
