

public class aufg1Methoden {
	public static void main(String[] args) {
		int[] zahlen= {1,6,4,8,4};
		System.out.println(convertArrayToString(zahlen));
	}

	public static String convertArrayToString(int[] zahlen) {
		String aus="";
		for (int i = 0; i < zahlen.length-1; i++) {
			aus+=zahlen[i]+", ";
		}
		aus+=zahlen[zahlen.length-1];
		return aus;
	}
}
