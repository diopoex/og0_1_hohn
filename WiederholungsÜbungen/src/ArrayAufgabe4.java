

import java.util.Scanner;

public class ArrayAufgabe4 {
  public static void main(String[] args) {
    Scanner input=new Scanner(System.in);
    System.out.println("Zu suchende Zahl: ");
    int x=input.nextInt();
    int[] zahlenArray = { 1, 5, 5, 7, 3, 11, 22, 23, 27 };
    boolean f=false;
    for (int i = 0; i < zahlenArray.length; i++) {
      if (zahlenArray[i]==x) {
        f=true;
      }
    }
    if (f) {
      System.out.println("Gefunden!");
    } else {
      System.out.println("Nicht Gefunden!");
    }
    input.close();
  }
}
