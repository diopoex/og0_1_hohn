

import java.util.Scanner;

public class Kontrollstrukturen_aufg1_PaulH�hn {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double mausPreis = 9;
		double rechnungsbetrag;
		System.out.println("Wieviel M�use m�chten sie bestellen?");
		int anz = input.nextInt();
		rechnungsbetrag = (mausPreis * anz)*1.19;
		if (anz < 10) {
			rechnungsbetrag += 10;
		}
		System.out.println("Sie m�ssen " + rechnungsbetrag + "� bezahlen");

		input.close();
	}
}
