
import java.util.Scanner;

public class Kontrollstrukturen_aufg5_2_PaulH�hn {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Berechnung des Quotienten und Rest �ber Subtraktion");
		System.out.println("Dividend");
		double dividend = input.nextDouble();
		double cDiv=dividend;
		System.out.println("Divisor");
		double divisor = input.nextDouble();
		double cDivisor=divisor;
		int i;
		for (i = 0; dividend >= divisor; i++) {
			dividend -= divisor;
		}
		System.out.println("Dividend: " + cDiv + "\nDivisor: " + cDivisor + "\nGanzzahliger Quotient " + i
				+ "\nGanzzahliger Rest: " + dividend);
		input.close();
	}
}
