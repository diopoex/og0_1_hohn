

import java.util.Scanner;

public class Kontrollstrukturen_aufg2_PaulH�hn {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Grenzwert eingeben: ");
		int n = input.nextInt();
		int zahl=0;
		for (int i = 2; i <= 2 * n; i += 2) {
			zahl+=i;
		}
		System.out.println(zahl);
		input.close();
	}
}
