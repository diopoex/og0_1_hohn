

import java.util.Scanner;

public class Kontrollstrukturen_aufg4_PaulH�hn {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Geben sie die zu pr�fende Zahl ein");
		long zahl = input.nextLong();
		boolean check = true;
		for (int i = 2; i < zahl; i++) {
			if (zahl % i == 0) {
				check = false;
				break;
			}
		}
		if (check) {
			System.out.println(zahl + " ist eine Primzahl");
		} else {
			System.out.println(zahl + " ist keine Primzahl");
		}
		input.close();
	}
}
