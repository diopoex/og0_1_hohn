

import java.util.Scanner;

public class Kontrollstrukturen_aufg5_PaulH�hn {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Berechnung des Quotienten und Rest �ber Subtraktion");
		System.out.println("Dividend");
		double dividend = input.nextDouble();
		System.out.println("Divisor");
		double divisor = input.nextDouble();
		double ergMitKomma = dividend / divisor;
		double ergOhneKomma = Math.floor(ergMitKomma);
		double rest = divisor * (ergMitKomma - ergOhneKomma);
		System.out.println("Dividend: " + dividend + "\nDivisor: " + divisor + "\nGanzzahliger Quotient " + ergOhneKomma
				+ "\nGanzzahliger Rest: " + rest);
		input.close();
	}
}
