

import java.util.Arrays;

public class aufg2Methoden {
	public static void main(String[] args) {
		int[] zahlen = { 1, 2, 3, 4, 5, 6 };
		System.out.println(Arrays.toString(umdrehenArray(zahlen)));
	}

	public static int[] umdrehenArray(int[] zahlen) {
		int x;
		for (int i = 0; i < zahlen.length / 2; i++) {
			x = zahlen[i];
			zahlen[i] = zahlen[zahlen.length - i - 1];
			zahlen[zahlen.length - i - 1] = x;
		}
		return zahlen;
	}
}