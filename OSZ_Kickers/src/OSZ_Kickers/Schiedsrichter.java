package OSZ_Kickers;

public class Schiedsrichter extends Person {
	private int gepfiffeneSpiele;

	public Schiedsrichter() {
		super();
	}

	public Schiedsrichter(int gepfiffeneSpiele, String name, long telefonnummer, boolean jahresbeitragBezahlt) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}
}
