package OSZ_Kickers;

public class Trainer extends Person {
	private String lizenzklasse;
	private double aufwantsentsch�digung;

	public Trainer() {
		super();
	}

	public Trainer(String lizenzklasse, double aufwantsentsch�digung, String name, long telefonnummer, boolean jahresbeitragBezahlt) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwantsentsch�digung = aufwantsentsch�digung;
	}

	public String getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(String lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public double getAufwantsentsch�digung() {
		return aufwantsentsch�digung;
	}

	public void setAufwantsentsch�digung(double aufwantsentsch�digung) {
		this.aufwantsentsch�digung = aufwantsentsch�digung;
	}

}
