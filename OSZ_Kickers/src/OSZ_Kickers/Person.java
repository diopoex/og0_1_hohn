package OSZ_Kickers;

public abstract class Person {
	private String name;
	private long telefonnummer;
	private boolean jahresbeitragBezahlt;

	public Person() {
		super();
	}

	public Person(String name, long telefonnummer, boolean jahresbeitragBezahlt) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(long telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean getJahresbeitragBezahlt() {
		return jahresbeitragBezahlt;
	}

	public void setJahresbeitragBezahlt(boolean jahresbeitragBezahlt) {
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}
}
