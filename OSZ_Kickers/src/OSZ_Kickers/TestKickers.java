package OSZ_Kickers;

public class TestKickers {
	public static void main(String[] args) {
		System.out.println("TESTPROGRAMM-OSZ_Kickers\n");

		Spieler spieler01 = new Spieler(7, "ZOM", "Alfred Meier", 492458623, true);
		Spieler spieler02 = new Spieler(91, "ST", "Gustav der Kleine", 492669187, false);

		Mannschaftsleiter mannschaftsleiter01 = new Mannschaftsleiter("Altlandsberg", 7.5, 19, "TW", "R�DIGER", 884889, true);

		Schiedsrichter schiedsrichter01 = new Schiedsrichter(0, "Herbert Meier", 44578, false);

		Trainer trainer01 = new Trainer("A", 50, "G�nther Vogt", 5458454, false);
		
		System.out.println("SPIELER01\nTrikotnummer: "+spieler01.getTrikotnummer()+"\nSpielposition: "+spieler01.getSpielposition()+"\nName: "+spieler01.getName()+"\nTel.: "+spieler01.getTelefonnummer()+"\nJahresbeitragBezahlt: "+spieler01.getJahresbeitragBezahlt());
		System.out.println("\nSPIELER02\nTrikotnummer: "+spieler02.getTrikotnummer()+"\nSpielposition: "+spieler02.getSpielposition()+"\nName: "+spieler02.getName()+"\nTel.: "+spieler02.getTelefonnummer()+"\nJahresbeitragBezahlt: "+spieler02.getJahresbeitragBezahlt());
		System.out.println("\nMANNSCHAFTSLEITER\nMannschafts-Name: "+mannschaftsleiter01.getNameMannschaft()+"\nRabatt: "+mannschaftsleiter01.getRabatt()+"\nSpielposition: "+mannschaftsleiter01.getSpielposition()+"\nName: "+mannschaftsleiter01.getName()+"\nTel.: "+mannschaftsleiter01.getTelefonnummer()+"\nJahresbeitragBezahlt: "+mannschaftsleiter01.getJahresbeitragBezahlt());
		System.out.println("\nSCHIEDSRICHTER\nGepfiffene-Spiele: "+schiedsrichter01.getGepfiffeneSpiele()+"\nName: "+schiedsrichter01.getName()+"\nTel.: "+schiedsrichter01.getTelefonnummer()+"\nJahresbeitragBezahlt: "+schiedsrichter01.getJahresbeitragBezahlt());
		System.out.println("\nTRAINER\nLizenzklasse: "+trainer01.getLizenzklasse()+"\nAufwantsentsch�digung: "+trainer01.getAufwantsentsch�digung()+"�"+"\nName: "+trainer01.getName()+"\nTel.: "+trainer01.getTelefonnummer()+"\nJahresbeitragBezahlt: "+trainer01.getJahresbeitragBezahlt());
	}
}
