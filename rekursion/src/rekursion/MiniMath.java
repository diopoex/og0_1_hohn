package rekursion;

import java.math.BigInteger;

public class MiniMath {
	
	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * @param n - Angabe der Zahl
	 * @return n!
	 */
	public static long berechneFakultaet(long n){
		if (n==0) {
			return 1;
		} else {
			return n*berechneFakultaet(n-1);
		}
	}
	
	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * @param n - Angabe der Potenz (max. 61 sonst Longoverflow)
	 * @return 2^n 
	 */
	public static BigInteger berechneZweiHoch(BigInteger n){
		if (n.compareTo(new BigInteger("1"))==0) {
			return new BigInteger("2");
		} else {
			return new BigInteger("2").multiply(berechneZweiHoch(n.subtract(new BigInteger("1"))));
		}
	}
	/*
	 public static int berechneZweiHoch(int n, int pot){
		if (pot==1) {
			return n;
		} else {
			return n*berechneZweiHoch(n,pot-1);
		}
	}
	 
	 */
	
	/**
	 * Die Methode berechnet die Summe der Zahlen von 
	 * 1 bis n (also 1+2+...+(n-1)+n)
	 * @param n - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static long berechneSumme(long n){
		if (n==1) {
			return 1;
		} else {
			return n+berechneSumme(n-1);
		}
	}
	
}

