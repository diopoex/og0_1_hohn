package rekursion;

import java.math.BigInteger;
/** MiniMathTest.java
*
* Testprogramm f�r verschiedene Berechnungen.
*
*/
import java.util.Scanner;

class MiniMathTest {

	public static void main(String arg[]) {

		Scanner myScanner = new Scanner(System.in);
		char operator;
		long n;
		System.out.println("Auswahl");
		System.out.println(" (Z)weierpotenz");
		System.out.println(" (F)akultaet ");
		System.out.println(" (S)umme ");
		System.out.println(" (E)xit ");
		do {
			System.out.print("Ihre Auswahl:\t");
			operator = myScanner.next().charAt(0);
			switch (operator) {
			case 'z':
				;
			case 'Z':
				System.out.print("     Exponent:  ");
				String t = myScanner.next();
				BigInteger bigInt=new BigInteger(t);

				System.out.println("\t\t2 ^ " + bigInt + " = " + MiniMath.berechneZweiHoch(bigInt) + "\n");
				break;
			case 'f':
				;
			case 'F':
				System.out.print("     Argument:  ");
				n = myScanner.nextLong();
				System.out.println("\t\t" + n + "! = " + MiniMath.berechneFakultaet(n) + "\n");
				break;
			case 's':
				;
			case 'S':
				System.out.print("     Argument:  ");
				n = myScanner.nextLong();
				System.out.println("\t\tSumme von 1 bis " + n + " = " + MiniMath.berechneSumme(n) + "\n");
				break;
			case 'e':
				;
			case 'E':
				myScanner.close();
				System.exit(0);
			} // switch
		} while (true);
	}// main
} // MiniMathTest