package rekursion;

public class kaninchenFibonacci {
	public static void main(String[] args) {
		System.out.println(fib(10));
		System.out.println(fibIter(10));
	}

	public static int fib(int n) {
		if (n == 1 || n == 0) {
			return n;
		} else {
			return fib(n - 1) + fib(n - 2);
		}
	}
	public static int fibIter(int n) {
		int n1=0;
		int n2=1;
		int n3=0;
		for (int i=0; i<n; i++) {
			n3=n2+n1;
			n2=n1;
			n1=n3;
		}
		return n3;
	}
}