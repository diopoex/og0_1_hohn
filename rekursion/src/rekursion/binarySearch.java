package rekursion;

public class binarySearch {

	public static void main(String[] args) {
		long[] arr = { 1, 5, 8, 9, 10, 15, 26, 50, 90, 100 };
		System.out.println(binarySearchS(arr, 0, arr.length - 1, 100));
	}

	public static int binarySearchS(long[] arr, int von, int bis, long x) {

		int m;
		if (von <= bis) {
			m = (von + bis) / 2;
			if (x == arr[m]) {
				return m;
			} else {
				if (x < arr[m]) {
					return binarySearchS(arr, von, m - 1, x);
				} else {
					return binarySearchS(arr, m + 1, bis, x);
				}
			}

		} else {
			return -1;
		}
	}
}
