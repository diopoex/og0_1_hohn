
public class Buch implements Comparable<Buch> {
	private String autor;
	private String titel;
	private String isbn;

	public Buch(String a, String t, String i) {
		this.autor = a;
		this.titel = t;
		this.isbn = i;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String toString() {
		return "[ Autor: " + this.autor + ", " + "Titel: " + this.titel + ", " + "ISBN: " + this.isbn + " ]";
	}

	public int compareTo(Buch b) {
		return this.titel.compareTo(b.getTitel());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (!(obj instanceof Buch))
			return false;
		Buch b = (Buch) obj;
		return this.autor.equals(b.autor) && this.titel.equals(b.titel) && this.isbn.equals(b.isbn);
		/*
		//nur vergleich isbn
		return this.isbn.equals(b.getIsbn());
		
		 */
	}

}
