package binarySearch;

public class MainBinarySearch {
	public static int zaehler = 0;

	public static void main(String[] args) {
		long[] arr = { 1, 5, 8, 9, 10, 15, 26, 50, 90, 100 };
		System.out.println(binarySearch(arr, 0, arr.length - 1, 60));
	}

	public static int binarySearch(long[] arr, int von, int bis, long x) {

		int m;
		zaehler++;
		if (von <= bis) {
			m = (von + bis) / 2;
			zaehler++;
			if (x == arr[m]) {
				return m;
			} else {
				zaehler++;
				if (x < arr[m]) {
					m = binarySearch(arr, von, m - 1, x);
				} else {
					m = binarySearch(arr, m + 1, bis, x);
				}
			}

		} else {
			m = zaehler;
		}
		return zaehler;
	}
}
