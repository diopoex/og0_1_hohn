package lineareSuche;

public class MainLineare {
	public static void main(String[] args) {
		
		long[] arrPlus2 = new long[20000000];
		long[] arrTimes2 = new long[20000000];
		for (int i = 0; i < arrPlus2.length; i++) {
			arrPlus2[i] = i * 2;
		}

		for (int i = 0; i < arrTimes2.length; i++) {
			arrTimes2[i]=i*i;
		}
		
		System.out.println("Lineare Suche Benötigte: \n"+lineareS(arrTimes2, 8));
		binarySearch.MainBinarySearch.zaehler=0;
		System.out.println("Binary Search Benötigte: \n"+binarySearch.MainBinarySearch.binarySearch(arrTimes2, 0, arrTimes2.length-1, 8));
		interpolationssuche.MainInterpolationssuche.zaehler=0;
		System.out.println("Interpolationsscuhe Benötigte: \n"+interpolationssuche.MainInterpolationssuche.interpolationssuche(arrTimes2, 0, arrTimes2.length-1, 8));
		
	}

	public static int lineareS(long[] arr, int x) {
		int zaehler=0;
		for (int i = 0; i < arr.length; i++) {
			zaehler++;
			if (arr[i] == x) {
				return zaehler;
			}
		}
		return zaehler;
	}

	public static void dummkopf(long[] arr) {
		for (int i = 0; i < 50; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}
