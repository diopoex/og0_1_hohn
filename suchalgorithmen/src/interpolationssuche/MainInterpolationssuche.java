package interpolationssuche;

public class MainInterpolationssuche {
	public static int zaehler=0;
	public static void main(String[] args) {
		long[] arr = { 2, 4, 8, 16, 32, 64, 100, 200, 300, 400 };
		//System.out.println(interpolationssuche(arr, 0, arr.length - 1, 100));
	}

	public static long interpolationssuche(long arr[], int von, int bis, int x) {
		//zaehler = 0;
		int t = (int) (von + (bis - von * (x - arr[von] / arr[bis] - arr[von])));
		zaehler++;
		if (arr[von] < arr[bis]) {
			zaehler++;
			if (x == arr[t]) {
				return t;
			} else if (x < arr[t]) {
				zaehler++;
				return interpolationssuche(arr, von, t - 1, x);
			} else {
				zaehler++;
				return interpolationssuche(arr, t + 1, bis, x);
			}
		} else if (x == arr[von]) {
			zaehler++;
			return zaehler;
		}
		zaehler++;
		return zaehler;
	}
}
