package rekursion;

public class MyMath implements IMyMath {

	@Override
	public int fakultaet(int zahl) {
		if (zahl==0) {
			return 1;
		} else {
			return zahl*fakultaet(zahl-1);
		}
	}

	@Override
	public int fibonacci(int zahl) {
		if (zahl == 1 || zahl == 0) {
			return zahl;
		} else {
			return fibonacci(zahl - 1) + fibonacci(zahl - 2);
		}
	}

}
