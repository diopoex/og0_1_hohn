package de.futurehome.tanksimulator;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MyChangeListener implements ChangeListener {
	public TankSimulator f;

	public MyChangeListener(TankSimulator f) {
		this.f = f;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		// TODO Auto-generated method stub
		JSlider slider = (JSlider) e.getSource();
		if (slider.getName().equals("schrittweite")) {
			f.myTank.setSchrittweiteValue(slider.getValue());
		}
	}

}
