package de.futurehome.tanksimulator;

public class Tank {

	private double fuellstand;
	private double prozent;
	private double progressValue;
	private int schrittweiteValue=1;

	public Tank(double fuellstand) {
		this.fuellstand = fuellstand;
	}

	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = fuellstand;
	}

	public void setProzent(double prozent) {
		this.prozent = prozent;
	}

	public double getProzent() {
		return prozent;
	}

	public int getProgressValue() {
		return (int) Math.ceil(progressValue);
	}

	public void setProgressValue(double progressValue) {
		this.progressValue = progressValue;
	}

	public int getSchrittweiteValue() {
		return schrittweiteValue;
	}

	public void setSchrittweiteValue(int schrittweiteValue) {
		this.schrittweiteValue = schrittweiteValue;
	}

}
