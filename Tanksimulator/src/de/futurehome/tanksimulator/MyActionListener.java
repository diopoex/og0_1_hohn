package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener/* , ChangeListener */ {
	public TankSimulator f;
	public int JSliderValue;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);//

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();
			if (fuellstand < 100) {
				fuellstand += f.myTank.getSchrittweiteValue();
				if (fuellstand > 100) {
					fuellstand = 100;
				}

				f.myTank.setFuellstand(fuellstand);
				f.myTank.setProzent(fuellstand);
				f.myTank.setProgressValue(fuellstand);

				f.lblFuellstand.setText("" + fuellstand);
				f.lblProzent.setText("" + f.myTank.getProzent() + "%");
				f.progress1.setValue(f.myTank.getProgressValue());
			}
		} else if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			if (!((fuellstand - f.myTank.getSchrittweiteValue()) < 0)) {
				fuellstand -= f.myTank.getSchrittweiteValue();
				f.myTank.setFuellstand(fuellstand);
				f.myTank.setProzent(fuellstand);
				f.myTank.setProgressValue(fuellstand);
				f.lblFuellstand.setText("" + fuellstand);
				f.progress1.setValue(f.myTank.getProgressValue());
				f.lblProzent.setText("" + f.myTank.getProzent() + "%");
			} // theoretische muss man "getProzent" und
				// "setProzent" nicht erstellen weil man an den
				// f�llstand auch einfach % ranh�ngen kann, habe es
				// aber aus gr�nden der oop und wiederholung gemacht
		} else if (obj == f.btnZuruecksetzen) {
			f.myTank.setFuellstand(0.0);
			f.lblFuellstand.setText("" + f.myTank.getFuellstand());
			f.myTank.setProzent(0.0);
			f.myTank.setProgressValue(0.0);
			f.lblProzent.setText("" + f.myTank.getProzent() + "%");
			f.progress1.setValue(f.myTank.getProgressValue());
		}
	}
	/*
	 * @Override public void stateChanged(ChangeEvent e) { // TODO Auto-generated
	 * method stub JSlider slider = (JSlider) e.getSource(); if
	 * (slider.getName().equals("schrittweite")) { JSliderValue = slider.getValue();
	 * }
	 * 
	 * }
	 */
}